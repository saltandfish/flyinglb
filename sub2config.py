import base64, re, json, socket, requests, os
import urllib.parse
from random import shuffle

# 运行流程在最下面
PROXIES = {"http": None, "https": None,}

# 归类是用的关键字和归类名，归类名无所谓，汉字也行
# Keywords and alias names, non ASCII characters are fine
ALIAS = {'港':'hk', '日':'jp', '美':'us'}

# A server should have at least one of these strings in 'remarks' section to be accepted
MUSTHAVE = ["IPLC", "中转"， "IEPL"]

# Binary file path
ssr_redir = './ssrr'
trojan_go = './trojan_go'
# Port setting
# baseport 代笔ssr和trojan redir监听的端口
# iptables_lb_baseport 是负载均衡所在的端口
baseport = 8964
iptables_lb_baseport = 1989
# current working directory
CWD = os.getcwd()+'/'


def decode_base64(string):
    pad_n = (4 -  (len(string) % 4)) % 4
    string += '=' * pad_n
    return base64.urlsafe_b64decode(string.encode('ascii')).decode('utf-8')


def doh(domain):
    res = requests.get('https://doh.dns.sb/dns-query?type=A&name={}'.format(domain))
    res = json.loads(res.text)
    return res['Answer'][0]['data']


def isOpen(ip,port):
    import signal
    def signal_handler(signum, frame):
        raise Exception("Timed out!")

    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(10)   # Ten or
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect((ip, int(port)))
        s.settimeout(5)
        s.shutdown(5)
        return True
    except:
        return False

def parse_feed(url, keywords=[]):
    def ssr(url):
        SSR_PATTERN = re.compile('(.*):(.*):(.*):(.*):(.*):(.*)/\\?')
        # "\(serverHost):\(serverPort):\(ssrProtocol):\(method):\(ssrObfs):"
        # obfsparam={base64(混淆参数(网址))}&protoparam={base64(混淆协议)}
        # &remarks={base64(节点名称)}&group={base64(分组名)})
        def get_param(url, config):
            param = urllib.parse.parse_qs(urllib.parse.urlsplit(url).query)
            key = {'obfsparam' : 'obfs_param', 'protoparam' : 'protocol_param', 'remarks' : 'remarks', 'group' : 'group'}
            for k,v in key.items():
                if param.get(k) != None:
                    config[v] = decode_base64(param[k][0].replace('-', '+').replace('_', '/'))
            return config

        url = decode_base64(url)
        config = {}
        first_match = SSR_PATTERN.match(url)
        if first_match == None:
            return False
        config['type'] = 'ssr'
        config['server'] = first_match.group(1)
        config['port'] = int(first_match.group(2))
        config['server_port'] = int(first_match.group(2))
        config['method'] = first_match.group(4)
        config['cipher'] = first_match.group(4)
        config['password'] = decode_base64(first_match.group(6))
        config['protocol'] = first_match.group(3)
        config['obfs'] = first_match.group(5)
        config = get_param(url, config)
        return config
    def ss(url):
        SS_PATTERN = re.compile('(.*):(.*)@(.*)(.*)#(.*)')
        def get_param(url, config):
            param = urllib.parse.parse_qs(urllib.parse.urlsplit(url).query)
            key = {'obfsparam' : 'obfs_param', 'protoparam' : 'protocol_param', 'remarks' : 'remarks', 'group' : 'group'}
            for k,v in key.items():
                if param.get(k) != None:
                    config[v] = decode_base64(param[k][0].replace('-', '+').replace('_', '/'))
            return config
        url = decode_base64(url)
        config = {}
        match = SS_PATTERN.match(url)
        config['type'] = 'ssr'
        config['server'] = match.group(3)
        config['port'] = int(match.group(4))
        config['server_port'] = int(match.group(4))
        config['method'] = match.group(1)
        config['password'] = match.group(2)
        config['remarks'] = match.group(6)
        config = get_param(url, config)
        return config

    def trojan(url):
        pattern = re.compile('(.*)@(.*):(.*)\?(.*)#(.*)')
        url = urllib.parse.unquote_to_bytes(url).decode()
        match = pattern.match(url)
        config = {}
        config['type'] = 'trojan'
        config['run_type'] = 'nat'
        config['local_addr'] = '127.0.0.1'
        config['server'] = match.group(2)
        config['remote_addr'] = match.group(2)
        config['port'] = int(match.group(3))
        config['remote_port'] = int(match.group(3))
        config['password'] = [match.group(1)]
        config['remarks'] = match.group(5).replace('\r', '')
        param = urllib.parse.parse_qs(urllib.parse.urlsplit(url).query)
        key = {'allowInsecure': 'allowInsecure', 'peer': 'peer', 'sni': 'sni'}
        for k,v in key.items():
            if param.get(k) == None:
                continue
            config[v] = param[k][0]
        config['ssl'] = {'verify': False, 'sni':config['sni']}
        return config

    def v2ray(json):
        pass

    print("Fetching server list")
    try:
        response = requests.get(url, timeout = 10, proxies = PROXIES)
    except:
        pass
    if response.ok:
        print("Fetched.")
        content = decode_base64(response.text)
    else:
        print("Cant fetch server list, something wen wrong, check your proxy might help.")
        return None
    servers = content.split("\n");
    list_config = []

    for server in servers:
        if len(server) == 0:
            continue

        match = PATTERN.match(server)

        if match == None:
            continue

        if 'trojan' in match.group(1):
            config = trojan(match.group(2))
            if not len(config['remarks']) > 1: continue
            list_config.append(config)
        elif 'ssr' in match.group(1):
            config = ssr(match.group(2))
            if not len(config) > 1: continue
            list_config.append(config)    
            
    shuffle(list_config)
    return list_config

def server_filter(pending_servers, keywords = []):
    shuffle(pending_servers)
    servers = []
    counter = 0
    def musthave(config):
        j = False
        for i in MUSTHAVE:
            if i in config['remarks']:
                return True

    for config in pending_servers:
        if len(config) < 1:
            continue
        if len(keywords[0]) == 0:
            break
        # Only get reliable servers
        if not musthave(config):
            continue
        if keywords[0] in config['remarks']:
            pending = config
            try:
                # DoH might be slower but far more secure
                panding['server'] = doh(panding['server'])
                #pending['server'] = socket.gethostbyname(pending['server'])
            except:
                continue
            try: 
                if not isOpen(pending['server'], pending['port']): continue
            except:
                continue
            counter += 1
            pending['name'] = '{}{}'.format(ALIAS[keywords[0]], counter)
            pending['location'] = ALIAS[keywords[0]]
            servers.append(pending)
            if counter>=5: 
                keywords.pop(0)
                counter = 0
    return servers

def gen_conf(server_list):
    from io import open
    def gen_supervisor_conf(server):
        temp = '''
[program:{}]
autorestart=true
user=root
command=sh -c "{}"\n
'''

        ssr_command = '{} -c {}{}'\
            .format(ssr_redir, CWD, '{}.json'.format(server['local_port']))
        trojan_command = '{} -config {}{}'\
            .format(trojan_go, CWD,'{}.json'.format(server['local_port']))
        if server['type'] == 'trojan':
            command = trojan_command
        elif server['type'] == 'ssr':
            command = ssr_command
        return temp.format(server['type']+str(server['local_port']), command)

    baseport = 8964
    iptables_lb_baseport = 1989
    spconf = ''

    iptables_lb = ''
    iptables_lb_template = '''#{}
iptables -A PREROUTING -t nat -p tcp -d 127.0.0.1 --dport {} \
-w -m statistic --mode nth --every 512 --packet 0 \
-j DNAT --to-destination 127.0.0.1:{}\n'''

    for i, server in enumerate(server_list):
        server['local_port'] = baseport + i
        server['udp'] = True
        print(server['type'], server['name'], server['server'], \
            server['local_port'], server['remarks'])
        server_json = json.dumps(server)
        f = open('{}.json'.format(server['local_port']), 'w')
        f.write(server_json)
        f.close()
        spconf += gen_supervisor_conf(server)
        if i==0 or (i!=0 and (server['location'] !=  location)):
            location = server['location']
            iptables_lb += iptables_lb_template.format(location, iptables_lb_baseport, server['local_port'])
            iptables_lb_baseport += 1
        else:
            iptables_lb += iptables_lb_template.format(location, iptables_lb_baseport-1, server['local_port'])


    f = open('sp.conf', 'w')
    f.write(spconf)
    f.close()

    f = open('lb.sh', 'w')
    f.write(iptables_lb)
    f.close

    return 0


subs = ['可以放好多个订阅地址在这里，目前只支持SSR/SS/TROJAN']
servers = []
for sub in subs:
    servers += parse_feed(sub)
    shuffle(servers)
shuffle(servers)
print('Got {} ssr servers before filtering'.format(len(servers)))
servers = server_filter(servers, list(ALIAS.keys()))
print('Got {} ssr servers after filtering'.format(len(servers)))
gen_conf(servers)