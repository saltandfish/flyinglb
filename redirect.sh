# Reset
iptables -t nat -F SSRT
iptables -t nat -X SSRT
iptables -t mangle -F SSRT
iptables -t mangle -X SSRT
iptables -t mangle -F DIVERT
iptables -t mangle -X DIVERT
# Haproxy tproxy
iptables -t mangle -N DIVERT
iptables -t mangle -A PREROUTING -p tcp -m socket -j DIVERT
iptables -t mangle -A DIVERT -j MARK --set-mark 1
iptables -t mangle -A DIVERT -j ACCEPT
ip rule add fwmark 1 lookup 100
ip route add local 0.0.0.0/0 dev lo table 100


iptables -t nat -N SSRT
iptables -t mangle -N SSRT
ip route add local default dev lo table 100
ip rule add fwmark 1 lookup 100
# Example: Google DNS
iptables -t mangle -A DIVERT -p tcp -d 8.8.4.4/32 --dport 53 -j TPROXY \
     --tproxy-mark 0x1/0x1 --on-port 1989 -w
iptables -t mangle -A DIVERT -p udp -d 8.8.4.4/32 --dport 53 -j TPROXY \
     --tproxy-mark 0x1/0x1 --on-port 1989 -w


# Private addresses
iptables -t nat -A SSRT -d 0.0.0.0/8 -j RETURN
iptables -t nat -A SSRT -d 10.0.0.0/8 -j RETURN
iptables -t nat -A SSRT -d 127.0.0.0/8 -j RETURN
iptables -t nat -A SSRT -d 169.254.0.0/16 -j RETURN
iptables -t nat -A SSRT -d 172.16.0.0/12 -j RETURN
iptables -t nat -A SSRT -d 192.168.0.0/16 -j RETURN
iptables -t nat -A SSRT -d 224.0.0.0/4 -j RETURN
iptables -t nat -A SSRT -d 240.0.0.0/4 -j RETURN

# Example: Return traffic 
#iptables -t nat -A SSRT -m set --match-set cn dst -j RETURN
#iptables -t nat -A SSRT -p tcp --syn -m connlimit --connlimit-above 32 -j RETURN

# Redir all requests
#iptables -t nat -A SSRT -p tcp -m set ! --match-set cn dst -j REDIRECT --to-ports 1989
#iptables -t nat -A SSRT -p tcp -j REDIRECT --to-ports 1989
#iptables -t nat -A SSRT -p udp -j REDIRECT --to-ports 1989

# Apply all rules
#iptables -t nat -I PREROUTING -p tcp -j SSRT
#iptables -t mangle -A PREROUTING -j SSRT

iptables -t nat -A OUTPUT -p tcp -j SSRT